import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Button, Container, Form, FormGroup, Input, Label} from 'reactstrap';
import AppNavbar from '../../AppNavbar';
import {ComboBox} from "@progress/kendo-react-dropdowns";
import _ from 'lodash';
import "../../App.css"

class ObjectiveEdit extends Component {

    emptyItem = {
        description: "",
        priority: "",
        implementer: ""
    };

    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem,
            isNew: false,
            selectedExecutorId: null,
            executors: [],
            comboboxExecutors: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getElement = this.getElement.bind(this);
    }

    componentDidMount() {
        this.handleGetObjective().then(response =>
            this.setState({
                item: response,
                isNew: !!response.id
            })
        );
        this.handleGetEmployee().then(response => {
                if (response) {
                    this.setState({
                        executors: response,
                        comboboxExecutors: _.map(response, (executor) => this.getComboboxItem(executor.id, executor.fullName))
                    })
                }
            }
        );
    }

    getComboboxItem(id, fullName) {
        return (id + ". " + fullName)
    }

    async handleGetObjective() {
        let id = this.props.match.params.id;
        if (id) {
            return await (
                await fetch(("/objective/" + id), {
                    method: "GET"
                })
            ).json();
        }
    }

    async handleGetEmployee() {
        return await (
            await fetch(("/employee/allEmployees"), {
                method: "GET"
            })
        ).json();
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }

    async handleSubmit(event) {
        event.preventDefault();
        await fetch("/objective", {
            method: (this.state.isNew) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.item),
        });
        this.props.history.push('/objective');
        this.back();
    }

    back() {
        window.history.back();
    }

    onChange = (event) => {
        let selectedExecutor = this.getElement(event)
        if (selectedExecutor) {
            this.setState({
                item: {
                    id: this.state.item.id,
                    description: this.state.item.description,
                    priority: this.state.item.priority,
                    implementer: selectedExecutor.id
                }
            })
        }
    };

    getElement(event) {
        return this.state.executors.find((executor) => this.getComboboxItem(executor.id, executor.fullName) === event?.value)
    }

    render() {
        return <div>
            <AppNavbar/>
            <Container>
                <h2>{this.state.isNew ? 'Изменить задачу' : 'Добавить задачу'}</h2>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="description">Описание</Label>
                        <Input type="text" name="description" id="description" value={ this.state.item.description || ''}
                               onChange={this.handleChange} autoComplete="description"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="implementer">Исполнитель</Label>
                        <br/>
                        <ComboBox
                            data={this.state.comboboxExecutors}
                            onChange={this.onChange}
                            onClick={(event) => this.setState({
                                id: this.getElement(event).id
                            })}
                        />

                       {/* <Input type="int"
                               name="implementer"
                               id="implementer"
                               value={this.state.selectedExecutorId || ''}/>*/}
                    </FormGroup>
                    <FormGroup>
                        <Label for="priority">Приоритет</Label>
                        <Input type="int" name="priority" id="priority" value={this.state.item.priority || ''}
                               onChange={this.handleChange} autoComplete="priority"/>
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary"
                                type="submit">
                            Сохранить
                        </Button>
                        <Button color="secondary"
                                onClick={() => this.back()}>
                            Назад
                        </Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(ObjectiveEdit);