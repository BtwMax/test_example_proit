import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from '../../AppNavbar';
import { Link } from 'react-router-dom';
import Pagination from "react-bootstrap-4-pagination";
import _ from 'lodash';

const MAX_PAGE_COUNT = 10;

class ObjectiveList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            objective: [],
            limit: 10,
            offset: 0,
            paging: {
                currentPage: 1,
                totalPages: 10,
                maxPageCount: 0
            }};
        this.remove = this.remove.bind(this);
        this.changePage = this.changePage.bind(this);
    }

    componentDidMount() {
        this.updateObjectiveList(this.state.limit, this.state.offset);
    }

    updateObjectiveList(limit, offset) {
        fetch(("/objective?limit=" + limit + "&offset=" + offset))
            .then(response => response.json())
            .then(response => this.setState({
                    objective: response.data,
                    limit: limit,
                    offset: (offset - 1) * this.state.limit,
                    paging: {
                        objective: response.data,
                        currentPage: this.state.paging.currentPage,
                        totalPages: response.totalCount,
                        maxPageCount: Math.min(MAX_PAGE_COUNT, Math.ceil(response.totalCount / this.state.limit))
                    }
                })
            );
    }

    async remove(id) {
        await fetch(('/objective/' + id), {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            }
        }).then(() => {
            let updatedObjective = [...this.state.objective].filter(i => i.id !== id);
            this.setState({objective: updatedObjective});
        });
    }

    changePage(page) {
        if (page <= 0 || page > this.state.paging.totalPages) {
            return
        }
        this.setState({
            paging: {
                totalPages: this.state.paging.totalPages,
                currentPage: page
            }
        });
        this.updateObjectiveList(this.state.limit, ((page - 1) * this.state.limit));
    }

    renderObjectiveList() {
        return _.map(this.state.objective, (objective) => {
            return <tr key={objective.id}>
                <td style={{whiteSpace: "nowrap"}}>{objective.description}</td>
                <td>{objective.fullName}</td>
                <td>{objective.priority}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link}
                                to={("/objective/" + objective.id)}>Изменить</Button>
                        <Button size="sm" color="danger" tag={Link}
                                to={("/objective/")}
                                onClick={() => this.remove(objective.id)}>Удалить</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });
        }

    render() {

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <div className="push-right">
                        <Button color="success" tag={Link} to="/objective/add/">Добавить новую задачу</Button>
                    </div>
                    <h3>Задачи</h3>
                    <Table className="mt-4">
                           <thead>
                           <tr>
                               <th width="35%">Описание</th>
                               <th width="35%">Исполнитель</th>
                               <th width="30%">Приоритет</th>
                           </tr>
                           </thead>
                        <tbody>
                            {this.renderObjectiveList()}
                        </tbody>
                    </Table>
                    <div className="App mr-5">
                        <Pagination
                            totalPages={this.state.paging.totalPages}
                            currentPage={this.state.paging.currentPage}
                            showMax={this.state.paging.maxPageCount}
                            activeBgColor="#198754"
                            activeBorderColor="#198754"
                            onClick={(page) => this.changePage(page)}
                            prevNext={false}
                        />
                    </div>
                </Container>
            </div>
        )
    }
}
export default ObjectiveList;