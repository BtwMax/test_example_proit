import React, {Component} from 'react';
import {Button, ButtonGroup, Container, Table} from 'reactstrap';
import AppNavbar from '../../AppNavbar';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import Pagination from "react-bootstrap-4-pagination";

const MAX_PAGE_COUNT = 10;

class EmployeeList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            employee: [],
            limit: 10,
            offset: 0,
            paging: {
                currentPage: 1,
                totalPages: 10,
                maxPageCount: 0
            }
        };
        this.remove = this.remove.bind(this);
        this.changePage = this.changePage.bind(this);
    }

    componentDidMount() {
        this.updateEmployeeList(this.state.limit, this.state.offset);
    }

    updateEmployeeList(limit, offset) {
        fetch(("/employee?limit=" + limit + "&offset=" + offset))
            .then(response => response.json())
            .then(response => this.setState({
                    employee: response.data,
                    limit: limit,
                    offset: (offset - 1) * this.state.limit,
                    paging: {
                        currentPage: this.state.paging.currentPage,
                        totalPages: response.totalCount,
                        maxPageCount: Math.min(MAX_PAGE_COUNT, Math.ceil(response.totalCount / this.state.limit))
                    }
                })
            );
    }

    async remove(id) {
        await fetch(('/employee/' + id), {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedEmployee = [...this.state.employee].filter(i => i.id !== id);
            this.setState({employee: updatedEmployee});
        });
    }

    changePage(page) {
        if (page <= 0 || page > this.state.paging.totalPages) {
            return
        }
        this.setState({
            paging: {
                totalPages: this.state.paging.totalPages,
                currentPage: page
            }
        });
        this.updateEmployeeList(this.state.limit, ((page - 1) * this.state.limit));
    }

    renderEmployeeList() {
        return _.map(this.state.employee, (employee) => {
            return <tr key={employee.id}>
                <td style={{whiteSpace: 'nowrap'}}>{employee.fullName}</td>
                <td>{employee.supervisor}</td>
                <td>{employee.branchName}</td>
                <td>{employee.taskCount}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link}
                                to={("/employee/" + employee.id)}>Изменить</Button>
                        <Button size="sm" color="danger" tag={Link}
                                to={("/employee/")}
                                    onClick={() => this.remove(employee.id)}>Удалить</Button>
                    </ButtonGroup>
                </td>
            </tr>
        })
    }

    render() {

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <div className="push-right">
                        <Button color="success" tag={Link} to="/employee/add/">Добавить сотрудника</Button>
                    </div>
                    <h3>Сотрудники</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="30%">ФИО</th>
                            <th width="30%">Руководитель</th>
                            <th width="30%">Филиал</th>
                            <th width="30%">Количество задач</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.renderEmployeeList()}
                        </tbody>
                    </Table>
                    <div className="App mr-5">
                        <Pagination
                            totalPages={this.state.paging.totalPages}
                            currentPage={this.state.paging.currentPage}
                            showMax={this.state.paging.maxPageCount}
                            activeBgColor="#198754"
                            activeBorderColor="#198754"
                            onClick={(page) => this.changePage(page)}
                            prevNext={false}
                        />
                    </div>
                </Container>
            </div>
        )
            ;
    }
}

export default EmployeeList;