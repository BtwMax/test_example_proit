import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Button, Container, Form, FormGroup, Input, Label} from 'reactstrap';
import AppNavbar from '../../AppNavbar';


class EmployeeEdit extends Component {

    emptyItem = {
        fullName: '',
        position: '',
        supervisor: '',
        branchName: '',
        taskCount: 0
    };

    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem,
            isNew: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.handleGetEmployee().then(response =>
            this.setState({
                item: response,
                isNew: !!response.id
            })
        );
    }

    async handleGetEmployee() {
        let id = this.props.match.params.id;
        if (id) {
            return await (
                await fetch(("/employee/" + id), {
                    method: "GET"
                })
            ).json();
        }
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const {item} = this.state;

        await fetch("/employee", {
            method: (this.state.isNew) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        });
        this.props.history.push('/employee');
        this.back();
    }

    back() {
        window.history.back();
    }

    render() {
        return <div>
            <AppNavbar/>
            <Container>
                <h2>{this.state.isNew ? 'Изменить сотрудника' : 'Добавить сотрудника'}</h2>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="fullName">ФИО</Label>
                        <Input type="text" name="fullName" id="fullName" value={this.state.item.fullName || ''}
                               onChange={this.handleChange} autoComplete="fullName"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="position">Должность</Label>
                        <Input type="text" name="position" id="position" value={this.state.item.position || ''}
                               onChange={this.handleChange} autoComplete="position"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="supervisor">Руководитель</Label>
                        <Input type="text" name="supervisor" id="supervisor" value={this.state.item.supervisor || ''}
                               onChange={this.handleChange} autoComplete="supervisor"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="branchName">Филиал</Label>
                        <Input type="text" name="branchName" id="branchName" value={this.state.item.branchName || ''}
                               onChange={this.handleChange} autoComplete="branchName"/>
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary"
                                type="submit">
                            Сохранить
                        </Button>
                        <Button color="secondary"
                                onClick={() => this.back()}>
                            Назад
                        </Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(EmployeeEdit);