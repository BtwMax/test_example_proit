import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ObjectiveList from './components/objective/ObjectiveList';
import ObjectiveEdit from "./components/objective/ObjectiveEdit";
import EmployeeList from "./components/employee/EmployeeList";
import EmployeeEdit from "./components/employee/EmployeeEdit";

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true} component={Home}/>
            <Route path='/employee' exact={true} component={EmployeeList}/>
            <Route path='/employee/:id' component={EmployeeEdit}/>
            <Route path='/objective' exact={true} component={ObjectiveList}/>
            <Route path='/objective/:id' component={ObjectiveEdit}/>
          </Switch>
        </Router>
    )
  }
}

export default App;
