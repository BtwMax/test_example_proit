package com.example.employees.service;

import com.example.employees.controllers.dto.ObjectiveDto;
import com.tej.JooQDemo.jooq.sample.model.Tables;
import com.tej.JooQDemo.jooq.sample.model.tables.records.ObjectiveRecord;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Result;
import org.springframework.stereotype.Service;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.row;

@Service
public class ObjectiveService {

    private static final Integer MAX_LIMIT = 10;

    private final DSLContext dslContext;

    public ObjectiveService(DSLContext dslContext) {

        this.dslContext = dslContext;
    }

    public int insertObjective(ObjectiveDto objective) {
        try {
            return dslContext.insertInto(Tables.OBJECTIVE,
                    Tables.OBJECTIVE.IMPLEMENTER,
                    Tables.OBJECTIVE.DESCRIPTION,
                    Tables.OBJECTIVE.PRIORITY)
                    .values(objective.getImplementer(),
                            objective.getDescription(),
                            objective.getPriority())
                    .execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    public int updateObjective(ObjectiveDto objective) {
        try {
            return dslContext.update(Tables.OBJECTIVE)
                    .set(row(Tables.OBJECTIVE.IMPLEMENTER,
                            Tables.OBJECTIVE.DESCRIPTION,
                            Tables.OBJECTIVE.PRIORITY),
                            row(objective.getImplementer(),
                                    objective.getDescription(),
                                    objective.getPriority()))
                    .where(Tables.OBJECTIVE.ID.eq(objective.getId()))
                    .execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }

    }

    public int deleteObjective(Long id) {
        try {
            return dslContext.delete(Tables.OBJECTIVE)
                    .where(Tables.OBJECTIVE.ID.eq(id))
                    .execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }

    }

    public Result<Record4<Long, String, String, Integer>> getObjectives(Integer limit, Integer offset) {
        Integer currentLimit = (limit != null && limit > 0) ? Math.min(limit, MAX_LIMIT) : MAX_LIMIT;
        Integer currentOffset = (offset != null && offset > 0) ? offset : 0;
        return dslContext
                .select(Tables.OBJECTIVE.ID,
                        Tables.OBJECTIVE.DESCRIPTION,
                        Tables.EMPLOYEE.FULL_NAME,
                        Tables.OBJECTIVE.PRIORITY)
                .from(Tables.OBJECTIVE)
                .innerJoin(Tables.EMPLOYEE)
                .on(Tables.EMPLOYEE.ID.eq(Tables.OBJECTIVE.IMPLEMENTER))
                .orderBy(Tables.OBJECTIVE.PRIORITY)
                .limit(currentLimit)
                .offset(currentOffset)
                .fetch();
    }

    public Integer getObjectiveCount() {
        Result<Record4<Long, String, String, Integer>> result = dslContext
                .select(Tables.OBJECTIVE.ID,
                        Tables.OBJECTIVE.DESCRIPTION,
                        Tables.EMPLOYEE.FULL_NAME,
                        Tables.OBJECTIVE.PRIORITY)
                .from(Tables.OBJECTIVE)
                .innerJoin(Tables.EMPLOYEE)
                .on(Tables.EMPLOYEE.ID.eq(Tables.OBJECTIVE.IMPLEMENTER))
                .fetch();
        return result.size();
    }

    public ObjectiveRecord getById(Long id) {
        return dslContext
                .selectFrom(Tables.OBJECTIVE)
                .where(Tables.OBJECTIVE.ID.eq(id))
                .fetchAny();
    }

    public Boolean checkPriority(int priority) {
        return priority > 0 && priority < 10;
    }
}
