package com.example.employees.service;

import com.example.employees.controllers.dto.EmployeeDto;
import com.tej.JooQDemo.jooq.sample.model.Tables;
import com.tej.JooQDemo.jooq.sample.model.tables.records.EmployeeRecord;
import org.jooq.*;
import org.springframework.stereotype.Service;


import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.row;

@Service
public class EmployeeService {

    private static final Integer MAX_LIMIT = 10;

    private final DSLContext dslContext;

    public EmployeeService(DSLContext dslContext) {

        this.dslContext = dslContext;
    }

    public int insertEmployee(EmployeeDto employee) {
        try {
            return dslContext.insertInto(Tables.EMPLOYEE,
                    Tables.EMPLOYEE.FULL_NAME,
                    Tables.EMPLOYEE.POSITION,
                    Tables.EMPLOYEE.SUPERVISOR,
                    Tables.EMPLOYEE.BRANCH_NAME)
                    .values(employee.getFullName(),
                            employee.getPosition(),
                            employee.getSupervisor(),
                            employee.getBranchName())
                    .execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    public int updateEmployee(EmployeeDto employee) {
        try {
            return dslContext.update(Tables.EMPLOYEE)
                    .set(row(Tables.EMPLOYEE.FULL_NAME, Tables.EMPLOYEE.SUPERVISOR),
                            row(employee.getFullName(), employee.getSupervisor()))
                    .where(Tables.EMPLOYEE.ID.eq(employee.getId()))
                    .execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    public int deleteEmployee(long id) {
        try {
            return dslContext.delete(Tables.EMPLOYEE)
                    .where(Tables.EMPLOYEE.ID.eq(id))
                    .execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return -1;
        }

    }

    public Result<Record5<Long, String, String, String, Integer>> getEmployees(Integer limit, Integer offset) {
        Integer currentLimit = (limit != null && limit > 0) ? Math.min(limit, MAX_LIMIT) : MAX_LIMIT;
        Integer currentOffset = (offset != null && offset > 0) ? offset : 0;
        return dslContext
                .select(Tables.EMPLOYEE.ID,
                        Tables.EMPLOYEE.FULL_NAME,
                        Tables.EMPLOYEE.SUPERVISOR,
                        Tables.EMPLOYEE.BRANCH_NAME,
                        count(Tables.OBJECTIVE.IMPLEMENTER))
                .from(Tables.EMPLOYEE)
                .leftOuterJoin(Tables.OBJECTIVE)
                .on(Tables.OBJECTIVE.IMPLEMENTER.eq(Tables.EMPLOYEE.ID))
                .groupBy(Tables.EMPLOYEE.ID)
                .orderBy(count(Tables.OBJECTIVE.IMPLEMENTER).desc())
                .limit(currentLimit)
                .offset(currentOffset)
                .fetch();
    }

    public Integer getEmployeesCount() {
        Result<Record1<Integer>> result = dslContext
                .select(count(Tables.EMPLOYEE))
                .from(Tables.EMPLOYEE)
                .leftOuterJoin(Tables.OBJECTIVE)
                .on(Tables.OBJECTIVE.IMPLEMENTER.eq(Tables.EMPLOYEE.ID))
                .groupBy(Tables.EMPLOYEE.ID)
                .fetch();
        return result.size();
    }

    public Result<EmployeeRecord> getAllEmployee() {
        return dslContext
                .selectFrom(Tables.EMPLOYEE)
                .orderBy(Tables.EMPLOYEE.ID)
                .fetch();
    }

    public EmployeeRecord getById(Long id) {
        return dslContext
                .selectFrom(Tables.EMPLOYEE)
                .where(Tables.EMPLOYEE.ID.eq(id))
                .fetchAny();
    }
}
