package com.example.employees.controllers.dto;

import java.util.ArrayList;

public class GetEmployeeElementResponse {
    private ArrayList<GetEmployeeElementResponseElement> data;
    private Integer totalCount;

    public GetEmployeeElementResponse() {
    }

    public GetEmployeeElementResponse(ArrayList<GetEmployeeElementResponseElement> data, Integer totalCount) {
        this.data = data;
        this.totalCount = totalCount;
    }

    public ArrayList<GetEmployeeElementResponseElement> getData() {
        return data;
    }

    public void setData(ArrayList<GetEmployeeElementResponseElement> data) {
        this.data = data;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return "GetEmployeeElementResponse{" +
                "data=" + data +
                ", totalCount=" + totalCount +
                '}';
    }
}
