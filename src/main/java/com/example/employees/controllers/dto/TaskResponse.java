package com.example.employees.controllers.dto;

public class TaskResponse {
    private Long id;
    private String description;
    private String fullName;
    private Integer priority;

    public TaskResponse() {
    }

    public TaskResponse(Long id, String description, String fullName, Integer priority) {
        this.id = id;
        this.description = description;
        this.fullName = fullName;
        this.priority = priority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }



    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "TaskResponse{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", fullName='" + fullName + '\'' +
                ", priority='" + priority + '\'' +
                '}';
    }
}
