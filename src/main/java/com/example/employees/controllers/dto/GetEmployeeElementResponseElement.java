package com.example.employees.controllers.dto;

public class GetEmployeeElementResponseElement {
    private Long id;
    private String fullName;
    private String supervisor;
    private String branchName;
    private Integer taskCount;

    public GetEmployeeElementResponseElement() {
    }

    public GetEmployeeElementResponseElement(Long id, String fullName, String supervisor, String branchName, Integer taskCount) {
        this.id = id;
        this.fullName = fullName;
        this.supervisor = supervisor;
        this.branchName = branchName;
        this.taskCount = taskCount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Integer getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(Integer taskCount) {
        this.taskCount = taskCount;
    }

    @Override
    public String toString() {
        return "GetEmployeeElementResponse{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", supervisor='" + supervisor + '\'' +
                ", branchName='" + branchName + '\'' +
                ", taskCount=" + taskCount +
                '}';
    }
}
