package com.example.employees.controllers.dto;

import java.util.ArrayList;

public class GetTaskResponseCount {
     private ArrayList<TaskResponse> data;
     private Integer totalCount;

    public GetTaskResponseCount() {
    }

    public GetTaskResponseCount(ArrayList<TaskResponse> data, Integer totalCount) {
        this.data = data;
        this.totalCount = totalCount;
    }

    public ArrayList<TaskResponse> getData() {
        return data;
    }

    public void setData(ArrayList<TaskResponse> data) {
        this.data = data;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return "GetTaskResponseCount{" +
                "data=" + data +
                ", totalCount=" + totalCount +
                '}';
    }
}
