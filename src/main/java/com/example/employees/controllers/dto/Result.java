package com.example.employees.controllers.dto;

public class Result {

    private Integer status;
    private String descriptions;

    public Result(Integer status, String descriptions) {
        this.status = status;
        this.descriptions = descriptions;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    @Override
    public String toString() {
        return "Result{" +
                "status=" + status +
                ", descriptions='" + descriptions + '\'' +
                '}';
    }
}
