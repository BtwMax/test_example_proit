package com.example.employees.controllers.dto;

public class ObjectiveDto {
    private Long id;
    private Long implementer;
    private String description;
    private Integer priority;

    public ObjectiveDto() {
    }

    public ObjectiveDto(Long id, Long implementer, String description, Integer priority) {
        this.id = id;
        this.implementer = implementer;
        this.description = description;
        this.priority = priority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getImplementer() {
        return implementer;
    }

    public void setImplementer(Long implementer) {
        this.implementer = implementer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "ObjectiveDto{" +
                "id=" + id +
                ", implementer=" + implementer +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                '}';
    }
}
