package com.example.employees.controllers.dto;

public class EmployeeDto {
    private Long id;
    private String fullName;
    private String position;
    private String supervisor;
    private String branchName;

    public EmployeeDto() {
    }

    public EmployeeDto(Long id, String fullName, String position, String supervisor, String branchName) {
        this.id = id;
        this.fullName = fullName;
        this.position = position;
        this.supervisor = supervisor;
        this.branchName = branchName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    public String toString() {
        return "EmployeeDto{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", position='" + position + '\'' +
                ", supervisor='" + supervisor + '\'' +
                ", branchName='" + branchName + '\'' +
                '}';
    }
}
