package com.example.employees.controllers;

import com.example.employees.controllers.dto.*;
import com.example.employees.service.ObjectiveService;
import com.tej.JooQDemo.jooq.sample.model.tables.records.EmployeeRecord;
import com.tej.JooQDemo.jooq.sample.model.tables.records.ObjectiveRecord;
import org.jooq.Record4;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/objective")
public class ObjectiveController {

    private final ObjectiveService objectiveService;

    public ObjectiveController(ObjectiveService objectiveService) {
        this.objectiveService = objectiveService;
    }

    @PostMapping
    public Result addObjective(@RequestBody ObjectiveDto objective) {
        if (objective.getImplementer() == null || objective.getDescription() == null || objective.getPriority() == null) {
            return new Result(0, "Не заполнены все обязательные поля");
        }
        if (!objectiveService.checkPriority(objective.getPriority())) {
            return new Result(0, "Значение приоритета не попадает в границы от 1 до 9");
        }
        int status = objectiveService.insertObjective(objective);
        return new Result(
                status,
                status > 0 ? "Задача добавлена" : "Задача не добавлена"
        );
    }

    @PutMapping
    public Result updateObjective(@RequestBody ObjectiveDto objective) {
        if (objective.getImplementer() == null || objective.getDescription() == null || objective.getPriority() == null) {
            return new Result(0, "Нельзя сохранять изменения с пустыми полями");
        }
        if (!objectiveService.checkPriority(objective.getPriority())) {
            return new Result(0, "Значение приоритета не попадает в границы от 1 до 9");
        }
        int status = objectiveService.updateObjective(objective);
        return new Result(
                status,
                status > 0 ? "Задача изменена" : "Задача не изменена"
        );
    }

    @DeleteMapping({"/{id}"})
    public Result deleteObjective(
            @PathVariable("id") Long id) {
        int status = objectiveService.deleteObjective(id);
        return new Result(
                status,
                status > 0 ? "Задача удалена" : "Задача не удалена"
        );
    }

    @GetMapping("/{id}")
    public ObjectiveDto getEmployeeById(@PathVariable("id") Long id) {
        ObjectiveRecord objective = objectiveService.getById(id);
        return new ObjectiveDto(
                objective.value1(),
                objective.value2(),
                objective.value3(),
                objective.value4()
        );
    }

    @GetMapping
    public GetTaskResponseCount objective(
            @PathParam("limit") Integer limit,
            @PathParam("offset") Integer offset
    ) {
        org.jooq.Result<Record4<Long, String, String, Integer>> result = objectiveService.getObjectives(limit, offset);
        ArrayList<TaskResponse> responseData = new ArrayList<>(List.of());
        result.forEach(element ->
                responseData.add(new TaskResponse(
                        element.value1(),
                        element.value2(),
                        element.value3(),
                        element.value4()
                ))
        );
        Integer objectiveCount = objectiveService.getObjectiveCount();
        return new GetTaskResponseCount(responseData, objectiveCount);
    }
}
