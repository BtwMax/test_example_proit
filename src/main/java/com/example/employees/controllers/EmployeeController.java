package com.example.employees.controllers;

import com.example.employees.controllers.dto.EmployeeDto;
import com.example.employees.controllers.dto.GetEmployeeElementResponse;
import com.example.employees.controllers.dto.GetEmployeeElementResponseElement;
import com.example.employees.controllers.dto.Result;
import com.example.employees.service.EmployeeService;
import com.tej.JooQDemo.jooq.sample.model.tables.records.EmployeeRecord;
import org.jooq.Record5;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping
    public Result addEmployee(@RequestBody EmployeeDto employee) {
        if (employee.getPosition() == null || employee.getBranchName() == null || employee.getFullName() == null) {
            return new Result(0, "Не заполнены все обязательные поля");
        }
        int status = employeeService.insertEmployee(employee);
        return new Result(
                status,
                status > 0 ? "Работник добавлен" : "Работник не добавлен"
        );
    }

    @PutMapping
    public Result updateEmployee(@RequestBody EmployeeDto employee) {
        if (employee.getFullName() == null) {
            return new Result(0, "Нельзя оставлять пустое поле имени");
        }
        int status = employeeService.updateEmployee(employee);
        return new Result(
                status,
                status > 0 ? "Работник изменен" : "Работник не изменен"
        );
    }

    @DeleteMapping({"/{id}"})
    public Result deleteEmployee(@PathVariable("id") Long id) {
        int status = employeeService.deleteEmployee(id);
        return new Result(
                status,
                status > 0 ? "Работник удален" : "Работник не удален"
        );
    }

    @GetMapping("/allEmployees")
    public List<EmployeeDto> getAllEmployee() {
        org.jooq.Result<EmployeeRecord> recordList = employeeService.getAllEmployee();
        List<EmployeeDto> records = new ArrayList<>(List.of());
        recordList.forEach(element ->
                records.add(new EmployeeDto(
                        element.value1(),
                        element.value2(),
                        element.value3(),
                        element.value4(),
                        element.value5()

                ))
        );
        return records;
    }

    @GetMapping("/{id}")
    public EmployeeDto getEmployeeById(@PathVariable("id") Long id) {
        EmployeeRecord employee = employeeService.getById(id);
        return new EmployeeDto(
                employee.value1(),
                employee.value2(),
                employee.value3(),
                employee.value4(),
                employee.value5()
        );
    }

    @GetMapping
    public GetEmployeeElementResponse getTaskCount(
            @PathParam("limit") Integer limit,
            @PathParam("offset") Integer offset
    ) {
        org.jooq.Result<Record5<Long, String, String, String, Integer>> result = employeeService.getEmployees(limit, offset);
        ArrayList<GetEmployeeElementResponseElement> responseData = new ArrayList<>(List.of());
        result.forEach(element ->
                responseData.add(new GetEmployeeElementResponseElement(
                        element.value1(),
                        element.value2(),
                        element.value3(),
                        element.value4(),
                        element.value5()
                ))
        );
        Integer employeeCount = employeeService.getEmployeesCount();
        return new GetEmployeeElementResponse(responseData, employeeCount);
    }

}
